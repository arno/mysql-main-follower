class AddsUniqIndexOnPostsDatesAndId < ActiveRecord::Migration[5.2]
  def change
    add_index :posts, [:id, :created_at, :updated_at]
  end
end
