class ConvertTableToUtf8mb4 < ActiveRecord::Migration[5.2]
  def change
    execute("ALTER TABLE `posts` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci")
  end
end
