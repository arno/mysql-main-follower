namespace :db do
  desc "Do 5 forks of 40 threads with a transactions of 10 inserts or deletes"
  task lots_of_concurrent_inserts: :environment do
    FORKS = 5
    THREADS = 5
    RUNS = 100
    TRANSACTION_SIZE = 10

    require 'set'
    require 'post'
    require 'ffaker'

    start_amount  =  Post.count
    fork_pid_pool = Set.new
    start_time = Time.now
    FORKS.times do
      fork_pid_pool << Process.fork do
        thread_pool = Set.new
        THREADS.times do
          thread_pool << Thread.new do
            RUNS.times do
              Post.transaction do
                TRANSACTION_SIZE.times do
                  [
                    -> { Post.create title: FFaker::Product.product_name, body: FFaker::Lorem.paragraph(5) },
                    # -> { Post.delete(Post.order("RAND()").limit(1).pluck(:id)) }
                  ].sample.call
                  print %w[. X o | - : \ / %].sample
                end
              end
            end
          end
        end
        thread_pool.map(&:join)
      end
    end

    fork_pid_pool.map{ |pid| Process.wait(pid) }
    puts "inserted or deleted %d rows, added  %d posts in %.4f seconds" % [FORKS * THREADS * RUNS * TRANSACTION_SIZE, (Post.count - start_amount), (Time.now - start_time)]
  end
end
